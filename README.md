This project is used to collect all of our needs in Software Developpement for our daily activity in Le Garage Numerique and also future projects.  

We need to split the work to be done in 3 phases:
1. Solve basic failures in our tools and processes that can be quickly adressed
2. Create an unified workflow / workspace
3. Imagine more elaborated tools for more advanced uses and services

Step 1: Solve Issues
=====================

## Tools we're using

We are currently using several tools:
- Nextcloud: deployed via Docker-compose, along with a Collabora Instance, on our [Gandi instance](https://nextcloud.legaragenumerique.fr)
- Framaboard: an instance of KanBoard, hosted by [Framasoft](https://framasoft.org) 
- Odoo: deployed and hosted on our [Gandi instance](https://odoo.legaragenumerique.fr)
- An e-mail server from gandi with Sogo Interface
- Gitlab: a Gold Instance (educative program) on which we host all our projects (https://gitlab.com/garagenum) 

## Tools we develop

We develop tools that helps us to repair people's computers and run workshops:
- a pxe server with custom debian (https://gitlab.com/garagenum/linux-admin-tools/debian-customizer)
- collection of mods for Minetest, an open source 3D game engine (www.minetest.net),  (https://gitlab.com/garagenum/minetest-lug900)

## What we are already using them for

### Nextcloud
Nextcloud is our main Workspace. Each member of the team has his own account - even trainees, volunteers, etc (<50 users).  
It contains great features such as an address book, an agenda, task management and file sharing (caldav/webdav) functionalities that we daily use.
We have also enabled Collabora, a tool dedicated to collaborative editing.

### Framaboard
This KanBan tool helps us to track the computers we repair and what has been/has to be done for each one.
Here is how it works:
- Each computer is represented by a card, with Serial as identifier. 
- Colmns in this Kanban define actual workflow (diagnosis, repair, OS install, deliver). 
- For each card, subtasks are used to describe what has to be done. Each subtask can be assigned to a memeber of the team when necessary. 
- Lastly, comments are used to describe problems and how they have been solved, to note down the computer owner's contact information, etc. 

### Odoo
We use it for accountancy only. 

### Sogo Gandi mail
We use it to access a shared inbox.

### Gitlab
We host our projects and documentation about our processes there. 
Our projects concern mostly 3 areas:
- Gaming and education, with a level editor in Minetest (https://gitlab.com/garagenum/minetest-lug9000), and a [command-line / video tutorial tool](http://garagenumeric.appspot.com/reader/read?type_item=story&story_id=story_20190429_114633_789300&page=player&embed=true)
- Linux administration, with a PXE server to install a customized version of Linux Debian with profiles for children, elderly people or gamers, depending on our users' needs (/https://gitlab.com/garagenum/linux-admin-tools/debian-customizer)
-  DevOps tools for websites ( https://gitlab.com/garagenum/webmaster ; https://gitlab.com/garagenum/tuto-master ; https://gitlab.com/garagenum/pdf-master ; https://gitlab.com/garagenum/gntutos)


## Improvements needed

### Nextcloud

#### An etherpad-lite integration! Most urgent!
We miss an etherpad instance that can be accessed within the Nextcloud etherpad app.
We are currently deploying our instance with docker-compose, based on [Wonderfall's image](https://github.com/Wonderfall/docker-nextcloud) but it can't be updated beyond v16 and doesn't have any etherpad-lite instance, so we need to redeploy our instance with an improved  docker-compose.yml .
https://gitlab.com/garagenum/internal/nextcloud_docker

#### using tags in Nextcloud Notes 
We use Notes a lot, this way we can write a kind of group diary. Minor and major events, meeting reports, working notes are also written within Notes. 
The current Nextcloud Notes app lacks a tagging feature, see https://github.com/nextcloud/notes/issues/299 .

#### Voice messages in Nextcloud Talk
We miss a voice message functionality because youngsters use to communicate this way and it's way faster than typing in a chat. 
We have raised this issue on Github: https://github.com/nextcloud/spreed/issues/1576

### Framaboard
Kanban (the software hosted on Framaboard) is OK and we use it for a long time, but we may change. Nextcloud does not offer a good Kanban app, so we're soon moving to Gitlab, unless we decide to use a classic ticketing software like osTicket.

### Sogo
The new email client that Gandi released to replace Roundcube is better than the old one. It's a Sogo instance. It includes a mini-CRM with agenda and contacts.  
We would like to replace the agenda with the one provided by Nextcloud via Caldav. We could then benefit from the "convert email to event" feature to get it sync with our Nextcloud workspace.

### Gitlab
- Gitlab seems a good candidate for replacing Kanboard and... as an email client! Gitlab will soon release a v12.8 that improves Service Desk, a feature that create issues from incoming emails and send comments back via email. As soon as some issues are [solved](https://gitlab.com/gitlab-org/gitlab/issues/7529), we will be able to use it. We are anxious to see it out, because it would adress a lot of issues we have with classic emails-clients. A email organised as Kanban, with tags, would be a lot easier to be managed by a team, instead of sending and forwarding everything to everyone! An exemple of this is dragapp.com 
We have already prepared the project fot that! ( https://gitlab.com/garagenum/contactatlegaragenumeriquepointfr)

### Our code

#### Minetest
Minetest is a sandbox game, like Minecraft. It is open-source, and exposes an API that we can easily use to create mods. 
With it , we created a game that let children discover the game little by little, with progressive levels and documentation.
We are actually developping a easy-to-use level editor, so that we can have a lot more of levels to share with childre.
We also started to teach code to youngsters with Minetest API. So they discover code with Lua, and they immediately see the result in a 3D env, and learn to developp along creating the scenario. (https://gitlab.com/garagenum/web/mtmod)
Issues need to be sorted, but you can see them at [project's page](https://gitlab.com/garagenum/minetest-lug9000/)

#### Debian-customizer
This is the tool we use everyday to install Linux on computers for people that come to us.
Its components are:
- a PXE Server, that provide debian installers and live images
- a full Debian configuration with scripts and custom conf files , according to the needs of our users.
We could benefit some improvements, lke migrating the server to iPxe that allows more, like multi-OS boot, Wireless boot, http boot image download, boot from usb ethernet adaptaters..
There are other issues that we started to work on at [project's page](https://gitlab.com/garagenum/linux-admin-tools/debian-customizer)

#### Online courses
What would be easy to implement and very useful is (https://gitlab.com/garagenum/gntutos) that lets share md files online with a nice and easy to maintain navigation.
originally it was a project from friends that hosted it on github and we didn't find the way to redirect correctly all links in order to deploy it on gitlab pages.

## What we could achieve with it
Once those problems are solved, we will be able to work better and quicker and to start running workshops in other associations/third places.

Step 2 : Build consistent apps and bridges
==========================================

The tools that we are using address a certain number of our needs but not all of them.
To be able to deploy our organization in other neighbourhoods, we need an unified workflow that covers more functionalities: 
- CRM: File Server, Agenda, Mail, Contacts
- Collaborative: Note taking and document editing, chat
- Project Management: Tasks, tracking of our interactions with people and of our work on computers.
- Learning Tools: tutorials and exercices for students and people at workshops

As we see it, this work could be done with Nextcloud apps. That way, it would be fully integrated to existing integrated workflows. Plus it would be based on an already improved engine, that we won't have to maintain or evolve.

We could deploy that tool for other non-lucrative orgs, so that they could have manage their activities using open-source softwares. 

## Apps to develop in Nextcloud
- Project Entry: Nextcloud 16 offers a way to connect chats, folders, decks (kanban instances) together, as a project. We woulde use an app that gathers projects, as an home page. You click on the project and then you access to an homepage of the project, that shows last cards in deck (kanban), last chat, last folders uploaded, last notes written, last events and e-mails concerned...
- YetAnother Notes app: with an creative approach of notes taking , as described at https://garagenum.gitlab.io/tutos/fragments/ (in french)
- aBetter e-mail client, with multiple accounts management and kanban view, ability to manage an adress across several users
- a ticketing app, that could be used for various cases: to track computers and maintenance operations, but also interactions with beneficiaries, for administrative procedures or support for professional integrations. Same as gitlab Support Desk, we could have emails getting in and out, and it would be also linked with caldav nextcloud agenda.
- an e-learning App
- a Bridge with Odoo, with timesheets retrieved from caldav calendar and fed in Odoo time tracking linked to cost accounting.


Step 3: Build more elaborated tools
===================================

In a second time, what we want is working on blockchains and smart contracts. We think that blockchain could be a great way to measure more efficiently and precisely our social impact. Exchanges with a local crypto-currency could also be used to emulate competition between students groups, that could promote their projects to collect crypto, so that they can have more computers, or more working time at Le Garage Numerique. 
What we would like is a decentralized blockchain, so each group can create its own blockchain, and internally play with it, before it becomes able to integrate main blockchain.
We already have quite functional code for a blockchain, but it's not public yet. We still miss few front end tools.

An other use of blockchain is creating applications. Indeed, while we speak of Project Management and issues tracking in our daily workflow (computer maintenance), there is an important feature that we would like to use but we avoided because of security and privacy issues: remote maintenance.
We don't want to be responsible for a great database that, if hacked, would possibly open access to computers that we repaired for people and will contain an agent. We think it would be a better idea to do that kind of stuff on a blockchain, so that everyone involved in the community takes part in the responsability, and security is done within blockchain's procedures.

